//
//  AppDelegate.swift
//  O Presente
//
//  Created by Gilson Gil on 26/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit
import FirebaseCore

@UIApplicationMain
final class AppDelegate: UIResponder, UIApplicationDelegate {
  var window: UIWindow?
  var autorization: AuthorizationLib!

  func application(_ application: UIApplication,
                   didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
    FirebaseApp.configure()
    autorization = AuthorizationLib.sharedManager() as? AuthorizationLib
    autorization.setEmail(Constants.PagSeguro.accountEmail,
                          withToken: Constants.PagSeguro.accountToken,
                          appName: Constants.PagSeguro.appName)
    return true
  }
}
