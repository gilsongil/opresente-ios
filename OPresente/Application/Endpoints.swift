//
//  Endpoints.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Foundation

struct Endpoints {
  static let baseURLString = "http://cumbuca.tk:8080/api/"
  static let baseURL = URL(string: Endpoints.baseURLString)

  static let bDay = "bday"
}
