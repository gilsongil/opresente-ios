//
//  BDayDetailsCell.swift
//  O Presente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit

final class BDayDetailsCell: UITableViewCell {
  @IBOutlet
  weak var nameLabel: UILabel!

  @IBOutlet
  weak var dateLabel: UILabel!

  @IBOutlet
  weak var descriptionLabel: UILabel!
}

// MARK: - UpdatableCell
extension BDayDetailsCell: UpdatableCell {
  func update(_ configurator: BDay) {
    nameLabel.text = configurator.name
    dateLabel.text = configurator.date
    descriptionLabel.text = configurator.description
  }
}
