//
//  GiftCell.swift
//  O Presente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit

final class GiftCell: UITableViewCell {
  @IBOutlet
  weak var titleLabel: UILabel!

  @IBOutlet
  weak var priceLabel: UILabel!
}

// MARK: - UpdatableCell
extension GiftCell: UpdatableCell {
  func update(_ configurator: Gift) {
    titleLabel.text = configurator.title
    priceLabel.text = String(format: "R$ %.2f", arguments: [configurator.value])
  }
}
