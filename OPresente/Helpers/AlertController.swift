//
//  AlertController.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

struct AlertController {
  static func alert(in viewController: UIViewController? = nil,
                    title: String? = nil,
                    message: String? = nil,
                    cancelTitle: String? = nil,
                    okTitle: String? = nil,
                    okAction: (() -> Void)? = nil) {
    DispatchQueue.main.async {
      let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
      if let cancelTitle = cancelTitle {
        let cancelAction = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
      }
      if let okAction = okAction {
        let action = UIAlertAction(title: okTitle, style: .default) { _ in
          okAction()
        }
        alertController.addAction(action)
      } else if cancelTitle == nil, let okTitle = okTitle {
        let cancelAction = UIAlertAction(title: okTitle, style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
      }
      if let viewController = viewController {
        viewController.present(alertController, animated: true, completion: nil)
      } else {
        UIApplication.shared.delegate?.window??.rootViewController?.present(alertController,
                                                                            animated: true,
                                                                            completion: nil)
      }
    }
  }

  static func alert(error: Error, in viewController: UIViewController? = nil, okAction: (() -> Void)? = nil) {
    switch error {
    case let networkError as NetworkError:
      alert(networkError: networkError, in: viewController, okAction: okAction)
    case let appError as AppError:
      alert(appError: appError, in: viewController, okAction: okAction)
    default:
      alert(in: viewController,
            message: error.localizedDescription,
            okTitle: "OK",
            okAction: okAction)
    }
  }

  static func alert(networkError error: NetworkError,
                    in viewController: UIViewController? = nil,
                    okAction: (() -> Void)? = nil) {
    alert(in: viewController,
          message: error.localizedDescription,
          okTitle: "OK",
          okAction: okAction)
  }

  static func alert(appError error: AppError,
                    in viewController: UIViewController? = nil,
                    okAction: (() -> Void)? = nil) {
    alert(in: viewController,
          message: error.localizedDescription,
          okTitle: "OK",
          okAction: okAction)
  }
}
