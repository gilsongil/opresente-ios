//
//  CompletionHandlerType.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Foundation

typealias CompletionHandlerType<T> = (() throws -> T) -> Void
