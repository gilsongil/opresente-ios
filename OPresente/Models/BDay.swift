//
//  BDay.swift
//  O Presente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Foundation

struct BDay: Codable {
  let name: String
  let description: String
  let date: String
  let gifts: [Gift]
  let user: User
  let beaconTag: String?
  let beaconMinor: Int?
  let beaconMajor: Int?

  static var mock: BDay {
    return BDay(name: "Aniversário do Jorginho",
                description: "Todos os anos Gael ganha muitos presentes, este ano gostaríamos de proporcionar uma viagem que marcará a vida dele e pedimos a ajuda de vocês para realizar este sonho.",
                date: "27 de Outubro de 2018",
                gifts: Gift.mock,
                user: User.mock,
                beaconTag: "E2C56DB5-DFFB-48D2-B060-D0F5A71096E0",
                beaconMinor: 5324,
                beaconMajor: 7682)
  }
}
