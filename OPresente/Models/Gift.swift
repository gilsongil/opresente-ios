//
//  Gift.swift
//  O Presente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Foundation

struct Gift: Codable {
  let identifier: String
  let title: String
  let description: String
  let value: Float

  static var mock: [Gift] {
    return [
      Gift(identifier: "1QG8ueih1",
           title: "Hospedagem na Disney",
           description: "1 diária no hotel da disney",
           value: 25),
      Gift(identifier: "hr891h3",
           title: "Cota de passagem para Orlando/EUA",
           description: "Cota no valor de 25 reais para passagem para Orlando/EUA",
           value: 50),
      Gift(identifier: "000",
           title: "Ajuda de custo",
           description: "Digite o valor que gostaria de contribuir",
           value: 0)
    ]
  }
}
