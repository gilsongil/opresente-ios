//
//  User.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Foundation

struct User: Codable {
  let identifier: Int
  let name: String
  let email: String
  let token: String

  static var mock: User {
    return User(identifier: 12378,
                name: "Gilson Gil",
                email: Constants.PagSeguro.accountEmail,
                token: Constants.PagSeguro.accountToken)
  }
}
