//
//  UpdatableCell.swift
//  O Presente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit

protocol UpdatableCell: class {
  associatedtype Configurator

  func update(_ configurator: Configurator)
}

struct CellConfigurator<Cell> where Cell: UpdatableCell {
  let configurator: Cell.Configurator
  let reuseIdentifier: String = NSStringFromClass(Cell.self).components(separatedBy: ".").last!

  func update(_ cell: UITableViewCell) {
    guard let cell = cell as? Cell else { return }
    cell.update(configurator)
  }

  func update(_ cell: UICollectionViewCell) {
    guard let cell = cell as? Cell else { return }
    cell.update(configurator)
  }
}

protocol CellConfiguratorType {
  var reuseIdentifier: String { get }

  func update(_ cell: UITableViewCell)
}

extension CellConfigurator: CellConfiguratorType {}
