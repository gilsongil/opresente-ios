//
//  PSTransparentLib.h
//  PSWalletLib
//
//  Created by TQI on 25/04/17.
//  Copyright © 2017 PagSeguro. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void(^ApprovalResultBlock)(BOOL approved, NSDictionary * success);
typedef void(^RefusedResultBlock)(NSError *error);

@protocol InstallmentsDelegate <NSObject>

-(void)installmentsInCard:(NSDictionary *)installments;

@end

@protocol SearchPostalCodeDelegate <NSObject>

-(void)searchPostalCodeRespose:(NSDictionary *)responseObject;

@end



@interface PSTransparentLib : NSObject

@property id <InstallmentsDelegate> delegateInstallments;

-(void)installmentsAmount:(NSString *)amount cardNumber:(NSString *)cardNumber;

@property id <SearchPostalCodeDelegate> delegatePostalCode;

- (void)searchPostalCode:(NSString *)postalCode;

-(void)psWalletDidFinishPayment;

/*O PagSeguro é apenas um gateway de pagamento e não realiza análise antifraude das transações. Assim, o chargeback é do vendedor e o PagSeguro não assume o risco sobre suas vendas. Caso esta modalidade faça sentido para o seu negócio e você queira utilizar o Gateway em seu aplicativo, é necessário:
 
        Entrar em contato pelo e-mail checkoutinapp@pagseguro.com.br, informando e-mail e telefone de contato;
        Ter a aprovação do PagSeguro;
        Assinar um contrato, onde estarão os termos que especificam a opção pelo uso do checkout gateway.*/

- (instancetype)initWithPayTransparentGatewayCreditCard:(NSString*)creditCard
                                               expMonth:(NSString *)expMonth
                                                expYear:(NSString *)expYear
                                                    cvv:(NSString *)cvv
                                          amountPayment:(NSString *)amountPayment
                                    quantityPaymentItem:(NSString *)quantityPaymentItem
                                   numberOfInstallments:(NSString *)numberOfInstallments
                                     descriptionPayment:(NSString *)descriptionPayment
                                                success:(ApprovalResultBlock)success
                                                failure:(RefusedResultBlock)failure ;


- (instancetype)initWithPayTransparentDefaultCreditCard:(NSString*)creditCard
                                               expMonth:(NSString *)expMonth
                                                expYear:(NSString *)expYear
                                                    cvv:(NSString *)cvv
                                      amountPaymentItem:(NSString *)amountPaymentItem
                                    quantityPaymentItem:(NSString *)quantityPaymentItem
                                   numberOfInstallments:(NSString *)numberOfInstallments
                                       valueInstallment:(NSString *)valueInstallment
                                     descriptionPayment:(NSString *)descriptionPayment
                                                  email:(NSString *)email
                                                   name:(NSString *)name
                                                    cpf:(NSString *)cpf
                                              birthDate:(NSString *)birthDate
                                          phoneAreaCode:(NSString *)phoneAreaCode
                                            phoneNumber:(NSString *)phoneNumber
                                                country:(NSString *)country
                                                  state:(NSString *)state
                                                   city:(NSString *)city
                                             postalCode:(NSString *)postalCode
                                               district:(NSString *)district
                                                 street:(NSString *)street
                                                 number:(NSString *)number
                                            extraAmount:(NSString *)extraAmount
                                             complement:(NSString *)complement
                                        notificationURL:(NSString *)notificationURL
                                                success:(ApprovalResultBlock)success
                                                failure:(RefusedResultBlock)failure;


- (instancetype)initWithTicketEmail:(NSString *)email
                               name:(NSString *)name
                                cpf:(NSString *)cpf
                      phoneAreaCode:(NSString *)phoneAreaCode
                        phoneNumber:(NSString *)phoneNumber
                      amountPayment:(NSString *)amountPayment
                quantityPaymentItem:(NSString *)quantityPaymentItem
                 descriptionPayment:(NSString *)descriptionPayment
                            country:(NSString *)country
                              state:(NSString *)state
                               city:(NSString *)city
                         postalCode:(NSString *)postalCode
                           district:(NSString *)district
                             street:(NSString *)street
                             number:(NSString *)number
                        extraAmount:(NSString *)extraAmount
                         complement:(NSString *)complement
                    notificationURL:(NSString *)notificationURL
                            success:(ApprovalResultBlock)success
                            failure:(RefusedResultBlock)failure;


@end



/*
 DESCRIÇÃO DE ERROS LIB
 
 1001 - INVALID CARD NUMBER (Número do cartão inválido);
 
 1002 - YEAR OF VALIDATION OF THE INVALID CARD (Ano de validade do cartão inválido);
 
 1003 - VALID MONTH OF INVALID CARD (Mês de validade do cartão inválido);
 
 1004 - VALUE OF INVALID PAYMENT (Valor do pagamento inválido);
 
 1005 - INVALID CVV NUMBER (Número do cvv inválido);
 
 9000 - NETWORK_ERROR (Falha de conexão);
 
 9001 - REFUSED_TRANSACTION_ERROR (Transação cancelada ou recusada);
 
 9002 - CREATE_TRANSACTION_ERROR (Falha ao criar transação);
 
 9003 - TIME_OUT_CHECK_TRANSACTION (Timeout verificação status da transação);
 
 9004 - CHECK_TRANSACTION_ERROR (Falha na verificação da transação);
 
 9005 - TIME_OUT_CHECK_TRANSACTION_VALIDATOR (Timeout verificação status da transação validadora);
 */
