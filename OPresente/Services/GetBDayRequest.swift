//
//  GetBDayRequest.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Alamofire

struct GetBDayRequest: URLRequestable {
  let code: String

  var method: HTTPMethod {
    return .get
  }

  var path: String {
    return Endpoints.bDay
  }

  var parameters: Parameters? {
    return [
      "id": code
    ]
  }

  var headers: Parameters? {
    return nil
  }

  var encoding: ParameterEncoding {
    return URLEncoding.queryString
  }

  func handleResponse(response: DataResponse<Any>, completion: @escaping CompletionHandlerType<Any?>) {
    guard let data = response.data else {
      completion { throw NetworkError.server }
      return
    }
    do {
      let bday = try JSONDecoder().decode(BDay.self, from: data)
      completion { return bday }
    } catch {
      completion { throw error }
    }
  }
}
