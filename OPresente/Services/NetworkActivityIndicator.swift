//
//  NetworkActivityIndicator.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit

final class NetworkActivityIndicator {
  static let shared = NetworkActivityIndicator()

  private var runningCount = 0

  static func start() {
    if shared.runningCount == 0 {
      DispatchQueue.main.async {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
      }
    }
    shared.runningCount += 1
  }

  static func stop() {
    shared.runningCount -= 1
    if shared.runningCount == 0 {
      DispatchQueue.main.async {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
      }
    }
  }
}
