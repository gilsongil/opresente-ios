//
//  NetworkError.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Foundation

enum NetworkError: AppError {
  case timeout
  case noConnection
  case server
  case unauthorized

  init?(statusCode: Int?, error: Error?) {
    if let statusCode = statusCode {
      switch statusCode {
      case 401:
        self = .unauthorized
        return
      default:
        break
      }
    }
    if let error = error {
      switch error._code {
      case NSURLErrorTimedOut:
        self = .timeout
        return
      case NSURLErrorNotConnectedToInternet:
        self = .noConnection
        return
      default:
        break
      }
    }
    return nil
  }

  var description: String {
    switch self {
    case .unauthorized:
      return "Ação não autorizada"
    default:
      return "Erro desconhecido"
    }
  }
}
