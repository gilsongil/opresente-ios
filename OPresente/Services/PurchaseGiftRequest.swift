//
//  PurchaseGiftRequest.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Alamofire

struct PurchaseGiftRequest: URLRequestable {
  let giftIdentifier: String
  let customValue: Float?
  let name: String?
  let email: String?

  var method: HTTPMethod {
    return .post
  }

  var path: String {
    return Endpoints.bDay
  }

  var parameters: Parameters? {
    var parameters: Parameters = [
      "identifier": giftIdentifier
    ]
    if let customValue = customValue {
      parameters["value"] = customValue
    }
    if let name = name {
      parameters["name"] = name
    }
    if let email = email {
      parameters["email"] = email
    }
    return parameters
  }

  var headers: Parameters? {
    return nil
  }

  var encoding: ParameterEncoding {
    return JSONEncoding.default
  }

  func handleResponse(response: DataResponse<Any>, completion: @escaping CompletionHandlerType<Any?>) {
    guard response.data != nil else {
      completion { throw NetworkError.server }
      return
    }
    completion { return }
  }
}

