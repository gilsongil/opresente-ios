//
//  URLRequestable.swift
//  OPresente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import Alamofire
import CoreTelephony

protocol URLRequestable: URLRequestConvertible {
  var method: HTTPMethod { get }
  var path: String { get }
  var parameters: Parameters? { get }
  var arrayParameters: [Parameters]? { get }
  var headers: Parameters? { get }
  var encoding: ParameterEncoding { get }

  func handleResponse(response: DataResponse<Any>, completion: @escaping CompletionHandlerType<Any?>)
}

extension URLRequestable {
  var arrayParameters: [Parameters]? { return nil }

  func asURLRequest() throws -> URLRequest {
    guard let url = Endpoints.baseURL?.appendingPathComponent(path) else { fatalError() }

    var urlRequest = URLRequest(url: url)
    urlRequest.httpMethod = method.rawValue
    headers?.forEach {
      guard let value = $0.value as? String else { return }
      urlRequest.addValue(value, forHTTPHeaderField: $0.key)
    }
    if let arrayParameters = arrayParameters, let jsonEncoding = encoding as? JSONEncoding {
      return try jsonEncoding.encode(urlRequest, withJSONObject: arrayParameters)
    } else {
      return try encoding.encode(urlRequest, with: parameters)
    }
  }

  @discardableResult
  func request(_ completion: @escaping CompletionHandlerType<Any?>) -> DataRequest {
    NetworkActivityIndicator.start()
    return SessionManager.default.request(self).responseJSON { response in
      #if DEBUG
      print("\n\n")
      print("--- [REQUEST] \(response.request?.httpMethod ?? "") - \(response.request?.url?.absoluteString ?? "")")
      print("--- [HEADER] \(response.request?.allHTTPHeaderFields ?? [:])")
      if let body = response.request?.httpBody {
        print("--- [BODY] \(String(data: body, encoding: String.Encoding.utf8) ?? "")\n")
      }
      if let status = response.response?.statusCode {
        print("--- [STATUS] \(status)\n")
      }
      if let res = response.result.value {
        print("--- [RESULT] \(res)\n")
      } else {
        print("--- [RESULT] EMPTY\n")
      }
      print("|____\n")
      #endif
      NetworkActivityIndicator.stop()
      if let networkError = NetworkError(statusCode: response.response?.statusCode,
                                         error: response.error) {
        completion { throw networkError }
        return
      }
      self.handleResponse(response: response, completion: completion)
    }
  }
}
