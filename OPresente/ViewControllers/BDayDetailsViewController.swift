//
//  BDayDetailsViewController.swift
//  O Presente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit
import CoreLocation

final class BDayDetailsViewController: UIViewController {
  @IBOutlet
  weak var tableView: UITableView!

  private var locationManager: CLLocationManager?

  private var beacon: CLBeaconRegion?

  var bDay: BDay! {
    didSet {
      configurators = [
        [
          CellConfigurator<BDayDetailsCell>(configurator: bDay)
        ],
        bDay.gifts.compactMap {
          CellConfigurator<GiftCell>(configurator: $0)
        }
      ]
      updateUI()
    }
  }

  var configurators: [[CellConfiguratorType]]?

  override func viewDidLoad() {
    super.viewDidLoad()

    setUpBeacon()
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    if let giftDetailsViewController = segue.destination as? GiftDetailsViewController {
      giftDetailsViewController.bDay = bDay
      giftDetailsViewController.gift = sender as? Gift
    }
  }
}

// MARK: - Private
private extension BDayDetailsViewController {
  func setUpBeacon() {
    guard let beaconTag = bDay.beaconTag,
      let uuidBeacon = UUID(uuidString: beaconTag),
      let beaconMajorValue = bDay.beaconMajor,
      let beaconMinorValue = bDay.beaconMinor,
      let beaconMajor = CLBeaconMajorValue(exactly: beaconMajorValue),
      let beaconMinor = CLBeaconMinorValue(exactly: beaconMinorValue) else { return }

    let identifier = bDay.name

    beacon = CLBeaconRegion(proximityUUID: uuidBeacon,
                            major: beaconMajor,
                            minor: beaconMinor,
                            identifier: identifier)

    locationManager = CLLocationManager()
    locationManager?.delegate = self
    locationManager?.requestAlwaysAuthorization()
    locationManager?.startMonitoring(for: beacon!)
    locationManager?.startRangingBeacons(in: beacon!)
  }

  func updateDistance(_ distance: CLProximity) {
    switch distance {
    case .immediate:
      navigationController?.navigationBar.barTintColor = .green
      navigationItem.title = "Entrada Confirmada"
    default:
      navigationController?.navigationBar.barTintColor = nil
      navigationItem.title = nil
    }
  }

  func updateUI() {
    DispatchQueue.main.async {
      self.tableView.reloadData()
    }
  }
}

// MARK: - UITableView DataSource
extension BDayDetailsViewController: UITableViewDataSource {
  func numberOfSections(in tableView: UITableView) -> Int {
    guard let configurators = configurators else { return 0 }
    return configurators.count
  }

  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    guard let configurators = configurators else { return 0 }
    return configurators[section].count
  }

  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let configurator = configurators?[indexPath.section][indexPath.row] else { return UITableViewCell() }
    let cell = tableView.dequeueReusableCell(withIdentifier: configurator.reuseIdentifier, for: indexPath)
    configurator.update(cell)
    return cell
  }
}

// MARK: - UITableView Delegate
extension BDayDetailsViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    guard indexPath.section > 0 else { return }
    let gift = bDay.gifts[indexPath.row]
    performSegue(withIdentifier: "SelectGift", sender: gift)
  }
}

// MARK: - CLLocationManager Delegate
extension BDayDetailsViewController: CLLocationManagerDelegate {
  func locationManager(_ manager: CLLocationManager, didRangeBeacons beacons: [CLBeacon], in region: CLBeaconRegion) {
    if let beacon = beacons.first {
      updateDistance(beacon.proximity)
    } else {
      updateDistance(.unknown)
    }
  }
}
