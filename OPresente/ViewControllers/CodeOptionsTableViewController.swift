//
//  CodeOptionsTableViewController.swift
//  OPresente
//
//  Created by Gilson Gil on 12/11/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit

protocol CodeOptionsTableViewControllerDelegate: class {
  func didSelectCode(_ code: String)
}

final class CodeOptionsTableViewController: UITableViewController {
  var codes: [String] = []

  weak var delegate: CodeOptionsTableViewControllerDelegate?
}

// MARK: - Table view data source
extension CodeOptionsTableViewController {
  override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return codes.count
  }

  override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
    cell.textLabel?.text = codes[indexPath.row]
    return cell
  }
}

// MARK: - Table view delegate
extension CodeOptionsTableViewController {
  override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    delegate?.didSelectCode(codes[indexPath.row])
  }
}
