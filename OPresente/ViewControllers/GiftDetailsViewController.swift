//
//  GiftDetailsViewController.swift
//  O Presente
//
//  Created by Gilson Gil on 27/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit

final class GiftDetailsViewController: UIViewController {
  @IBOutlet
  weak var titleLabel: UILabel!

  @IBOutlet
  weak var descriptionLabel: UILabel!

  @IBOutlet
  weak var valueLabel: UILabel!

  var psWallet: PSWallet!

  var bDay: BDay! {
    didSet {
      let autorization = AuthorizationLib.sharedManager() as! AuthorizationLib
      autorization.setEmail(bDay.user.email,
                            withToken: bDay.user.token,
                            appName: Constants.PagSeguro.appName)
    }
  }

  var gift: Gift! {
    didSet {
      guard psWallet != nil else { return }
      psWallet.amountItemPaymentBtnPagSeguro = String(format: "%.2f", arguments: [gift.value])
      psWallet.descriptionPaymentBtnPagSeguro = gift.title
      psWallet.itemQuantityPaymentBtnPagSeguro = 1
    }
  }

  override func viewDidLoad() {
    super.viewDidLoad()

    psWallet = PSWallet(parentViewController: self)
    psWallet.delegate = self
    let viewButton = psWallet.createButtonPagSeguro()
    viewButton.translatesAutoresizingMaskIntoConstraints = false
    view.addSubview(viewButton)

    view.addConstraint(viewButton.topAnchor.constraint(equalTo: valueLabel.bottomAnchor, constant: 100))
    view.addConstraint(viewButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 20))
    view.addConstraint(viewButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: -20))
    viewButton.addConstraint(viewButton.heightAnchor.constraint(equalToConstant: 100))

    if let gift = gift {
      psWallet.amountItemPaymentBtnPagSeguro = String(format: "%.2f", arguments: [gift.value])
      psWallet.descriptionPaymentBtnPagSeguro = gift.title
      psWallet.itemQuantityPaymentBtnPagSeguro = 1

      titleLabel.text = gift.title
      descriptionLabel.text = gift.description
      valueLabel.text = String(format: "%.2f", arguments: [gift.value])
    }
  }

  deinit {
    psWallet.logout()
  }
}

// MARK: - PSWallet Delegate
extension GiftDetailsViewController: PSWalletDelegate {
  func psWalletDidFinishGetUserMainCard(_ userMainCard: PSWalletCard?) {
    print(userMainCard!)
  }

  func psWalletDidFailUserMainCardWithError(_ error: Error?) {
    print(error!)
  }

  func psWalletDidFinishPayment(withSuccess success: [AnyHashable : Any]?) {
    print(success!)
    PurchaseGiftRequest(giftIdentifier: gift.identifier, customValue: nil, name: nil, email: nil).request { _ in }
  }

  func psWalletDidFailPaymentWithError(_ error: Error?) {
    print(error!)
  }
}
