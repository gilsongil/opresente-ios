//
//  SelectBdayViewController.swift
//  O Presente
//
//  Created by Gilson Gil on 26/08/18.
//  Copyright © 2018 Presente. All rights reserved.
//

import UIKit
import GoogleMobileVision
import FirebaseMLVision

final class SelectBDayViewController: UIViewController {
  @IBOutlet
  weak var codeTextField: UITextField!

  override func viewWillAppear(_ animated: Bool) {
    super.viewWillAppear(animated)
    navigationController?.navigationBar.barTintColor = nil
  }

  override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
    super.prepare(for: segue, sender: sender)
    if let viewController = segue.destination as? BDayDetailsViewController {
      viewController.bDay = sender as? BDay
    } else if let viewController = segue.destination as? CodeOptionsTableViewController,
      let codes = sender as? [String] {
      viewController.codes = codes
      viewController.delegate = self
    }
  }
}

// MARK: - Actions
extension SelectBDayViewController {
  @IBAction
  func advanceTapped() {
    guard let code = codeTextField.text, !code.isEmpty else { return }
    GetBDayRequest(code: code).request { [weak self] callback in
      do {
        let bday = try callback() as? BDay
        self?.performSegue(withIdentifier: "SelectBday", sender: bday)
      } catch {
        AlertController.alert(error: error)
      }
    }
  }

  @IBAction
  func scanTapped() {
    let imagePicker = UIImagePickerController()
    imagePicker.sourceType = .camera
    imagePicker.delegate = self
    present(imagePicker, animated: true, completion: nil)
  }
}

// MARK: - Private
private extension SelectBDayViewController {
  func analyzeImage(_ image: UIImage) {
    let vision = Vision.vision()
    let textRecognizer = vision.cloudTextRecognizer()
    let visionImage = VisionImage(image: image)
    textRecognizer.process(visionImage) { [weak self] text, error in
      if let error = error {
        AlertController.alert(error: error)
      } else {
        let codes: [String] = text?.blocks.compactMap {
          let text = $0.text.filter ({ $0 != " " })
          guard text.count == 15 else { return nil }
          return text
        } ?? []
        switch codes.count {
        case 0:
          AlertController.alert(in: self, title: "Nenhum código válido encontrado", cancelTitle: "Ok")
        case 1:
          DispatchQueue.main.async {
            self?.codeTextField.text = codes[0]
            self?.advanceTapped()
          }
        default:
          DispatchQueue.main.async {
            self?.performSegue(withIdentifier: "CodeOptions", sender: codes)
          }
        }
      }
    }
  }
}

// MARK: - UITextField Delegate
extension SelectBDayViewController: UITextFieldDelegate {
  func textFieldShouldReturn(_ textField: UITextField) -> Bool {
    textField.resignFirstResponder()
    return true
  }
}

// MARK: - UIImagePickerController Delegate
extension SelectBDayViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
  func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
    guard let image = info[UIImagePickerControllerOriginalImage] as? UIImage else {
      dismiss(animated: true, completion: nil)
      return
    }
    analyzeImage(image)

    dismiss(animated: true, completion: nil)
  }
}

// MARK: - CodeOptionsTableViewController Delegate
extension SelectBDayViewController: CodeOptionsTableViewControllerDelegate {
  func didSelectCode(_ code: String) {
    navigationController?.popViewController(animated: true)
    codeTextField.text = code
    advanceTapped()
  }
}
